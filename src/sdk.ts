import crypto from 'crypto'
import { partisiaCrypto } from 'partisia-blockchain-applications-crypto'
import * as bson from 'bson'
import assert from 'assert'
import { PermissionTypes, sdkListenTabEvent, sdkOpenPopupTab } from './sdk-listeners'

declare global {
  interface Window {
    __onPartisiaConfirmWin: Function
    __onPartisiaWalletTabEvent: Function
  }
}
export interface ISdkConnection {
  account: { address: string; pub: string }
  popupWindow: { tabId: number; box: string }
}
export class PartisiaSdk {
  readonly seed: string
  private _connection?: ISdkConnection = undefined

  constructor(args: { seed?: Buffer | string; connection?: ISdkConnection } = {}) {
    const { seed, connection } = args
    if (seed) {
      this.seed = typeof seed === 'string' ? seed : Buffer.from(seed).toString('hex')
      if (connection) {
        this._connection = connection
      }
    } else {
      this.seed = crypto.randomBytes(32).toString('hex')
    }
  }

  async connect(args: { permissions: PermissionTypes[]; dappName: string; chainId: string }) {
    assert(
      typeof window.__onPartisiaConfirmWin === 'function' &&
      typeof window.__onPartisiaWalletTabEvent === 'function',
      'Extension not Found'
    )

    const hd = partisiaCrypto.wallet.getKeyPairHD(partisiaCrypto.wallet.entropyToMnemonic(this.seed), 0)

    const payloadOpenConfirm = {
      payload: args,
      windowType: 'connection',
      xpub: hd.xpub,
      msgId: hd.publicKey,
    }

    const popupWindow = await sdkOpenPopupTab<{ tabId: number; box: string }>(payloadOpenConfirm)
    const { payload } = await sdkListenTabEvent<{ tabId: number; payload: string }>(popupWindow.tabId)

    const account = bson.deserialize(partisiaCrypto.wallet.decryptMessage(hd.privateKey, payload)) as {
      address: string
      pub: string
    }
    this._connection = { account, popupWindow }
  }
  async signMessage(args: { contract?: string, payload: string, payloadType: 'utf8' | 'hex' | 'hex_payload', dontBroadcast?: boolean }) {
    assert(this.isConnected, 'You must connect the Dapp first')
    const { payload, payloadType, contract, dontBroadcast } = args
    const obj = { payload, payloadType, dontBroadcast }
    if (payloadType === 'hex_payload') {
      assert(typeof contract === 'string' && contract?.length === 42, 'must supply Contract for hex_payload type')
      obj.payload = `${contract}${obj.payload}`
    }

    const publicKey = this.connection!.popupWindow.box
    const enc = partisiaCrypto.wallet.encryptMessage(publicKey, bson.serialize(obj)).toString('hex')

    const hd = partisiaCrypto.wallet.getKeyPairHD(partisiaCrypto.wallet.entropyToMnemonic(this.seed), 0)
    const payloadWindowSign = {
      payload: enc,
      windowType: PermissionTypes.SIGN,
      connId: hd.publicKey,
    }
    const popupSign = await sdkOpenPopupTab<{ tabId: number }>(payloadWindowSign)
    const popupData = await sdkListenTabEvent<{ tabId: number; payload: string, hd_idx: number }>(popupSign.tabId)
    const hdWallet = partisiaCrypto.wallet.getKeyPairHD(partisiaCrypto.wallet.entropyToMnemonic(this.seed), popupData.hd_idx)

    const res = bson.deserialize(partisiaCrypto.wallet.decryptMessage(Buffer.from(hdWallet.privateKey, 'hex'), popupData.payload)) as {
      signature: string
      serializedTransaction: string
      digest: string
      trxHash: string
      isFinalOnChain: boolean
    }
    return res
  }
  async requestKey() {
    assert(this.isConnected, 'You must connect the Dapp first')

    const hd = partisiaCrypto.wallet.getKeyPairHD(partisiaCrypto.wallet.entropyToMnemonic(this.seed), 0)
    const payloadWindow = {
      payload: '',
      windowType: PermissionTypes.PRIVATE_KEY,
      connId: hd.publicKey,
    }
    const popupKey = await sdkOpenPopupTab<{ tabId: number }>(payloadWindow)
    const popupData = await sdkListenTabEvent<{ tabId: number; payload: string, hd_idx: number }>(popupKey.tabId)
    const hdWallet = partisiaCrypto.wallet.getKeyPairHD(partisiaCrypto.wallet.entropyToMnemonic(this.seed), popupData.hd_idx)

    const res = bson.deserialize(partisiaCrypto.wallet.decryptMessage(Buffer.from(hdWallet.privateKey, 'hex'), popupData.payload)) as {
      address: string,
      public_key: string,
      private_key: string,
    }
    return res
  }
  get isConnected() {
    return !!this._connection
  }
  get connection() {
    return this._connection
  }
}
